#ifndef __MP_H_
#define __MP_H_
#include <string>
#include "Base.h"
using namespace std;

class MaterialPool{
 private:
    Ogre::SceneManager *mSceneMgr;
    struct node{                /* 循环链表，存储资源池 */
        Entity *data;
        node *prev;
        node *next;
    };
    int numAll;                 /* 资源实体数量，用于命名的时候不重名 */
    int type;                   /* 资源的类形，用于是否 GameOver 判断 */
    int direction;              /* 此类资源对方向的改变量 0不变 -1左 1右 */
    int matrix[80][80];         /* 逻辑矩阵 */
    int centerx, centery;       /* 逻辑矩阵中心点 */
    node *h, *t;
    std::string name;           /* 主资源名称 */
    std::string mesh;           /* 资源对应的 mesh 文件 */
    std::string getTmpName();   /* 根据主资源名派生实体资源名称 */
    void add();                 /* 生成新的资源实体 */
 public:
    /* mSceneMgr, name, mesh, type, direction */
    MaterialPool(Ogre::SceneManager*, std::string);
    ~MaterialPool();
    Entity* get();              /* 返回一个资源实体 */
    Entity* rel();              /* 释放资源实体 */
    int getDire();              /* 返回 direction */
    int getType();              /* 返回 type */
    int checkState(int, int);   /* 判断当前相对坐标的状态 */
};
#endif
