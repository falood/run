#ifndef __CHARACTER_H_
#define __CHARACTER_H_

#include "Base.h"

class Character
{
 private:
  enum{ FORWARD, LEFT, BACKWARD, RIGHT,
        ALIVE = 0x1, 
        MOVE_LEFT = 0x10, 
        MOVE_RIGHT = 0x100,
        TURN_LEFT = 0x1000,
        TURN_RIGHT = 0x10000,
        };
  Entity* mSword1;
  Entity* mSword2;
  Entity* mHero;

  SceneNode* mHeroNode;
  SceneNode* mNode;
  SceneNode* mCameraNode;
  Camera* mCamera;

  Vector3 mDirection[4];
  int mDirId;
  //int mAction;
  int mActionTime;
  //int mMove;
  //bool mAlive;
  int mState;

  enum{NUM_ANIMS = 13};                 //total nums of animations
  Real mSpeed;                          // animation speed
  AnimationState* mAnims[NUM_ANIMS];    // animation 


 public:
  Character(SceneManager* sm, Camera* camera);
  ~Character(void);

  void injectKeyDown( const OIS::KeyEvent& evt );
  void injectKeyUp( const OIS::KeyEvent& evt );
  void injectMouseDown( const OIS::MouseEvent& evt );
  void injectMouseUp( const OIS::MouseEvent& evt );
  void injectMouseMove( const OIS::MouseEvent& evt );

  void update(Real dt);
  void setSpeed(Real speed);
  void turnLeft();
  void turnRight();
  void moveLeft();
  void moveRight();
  Vector3 getPosition();
  void die();
 private:
  void act();
};

#endif //__CHARACTER_H_
