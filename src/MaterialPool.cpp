#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include "MaterialPool.h"

MaterialPool::MaterialPool(Ogre::SceneManager *mSceneMgr, std::string cfg){
    int i, j;
    char ch;
    // 从配置文件读参数和逻辑矩阵
    int row, col;
    ifstream file_in;
    file_in.open(cfg.c_str(), ios::in);
    file_in >> name >> mesh >> type >> direction >> row >> col;
    file_in >> centerx >> centery;
    file_in.get(ch);
    for (i = 0; i < row; i++){
        for (j = 0; j < col; j++){
            file_in.get(ch);
            matrix[i][j]  = ch - 48;
        }
        file_in.get(ch);
    }
    // file_in.close();
    // 变量初始化
    numAll = 0;
    this->mSceneMgr = mSceneMgr;
    h = t = NULL;
    // 每个资源预置 20 份
    for(int i=0; i<20; i++){
        add();
    }
    // 指针回归初始状态
    h = t;
}

MaterialPool::~MaterialPool(){
    // 释放链表
    node *tmpNode;
    while (NULL != t){
        tmpNode = t->next;
        t->next = tmpNode->next;
        delete tmpNode;
        tmpNode = NULL;
    }
    h = t = NULL;
}

std::string MaterialPool::getTmpName(){
    std::stringstream strout;
    strout << numAll + 1 << name;
    return strout.str();
}

void MaterialPool::add(){
    std::string tmpName;
    node *tmpNode = new node;
    tmpNode->prev = tmpNode;
    tmpNode->next = tmpNode;
    tmpName = getTmpName();
    tmpNode->data = mSceneMgr->createEntity(tmpName, mesh);
    if (0 == numAll){
        h = t = tmpNode;
    }
    // 设置循环链表
    tmpNode->prev = h;
    tmpNode->next = h->next;
    h->next->prev = tmpNode;
    h->next = tmpNode;
    h = tmpNode;
    numAll += 1;
    // 释放指针
    tmpNode = NULL;
    delete tmpNode;
}
Entity* MaterialPool::get(){
    Entity *tmpEntity;
    tmpEntity = h->data;
    // 若没有候选资源可用，动态添加一个
    if (h->next == t){
        add();
    }else{
        h = h->next;
    }
    return tmpEntity;
}
Entity* MaterialPool::rel(){
    Entity *tmpEntity;
    tmpEntity = t->data;
    t = t->next;
    return tmpEntity;
}
int MaterialPool::getDire(){
    return direction;
}
int MaterialPool::getType(){
    return type;
}
int MaterialPool::checkState(int x, int y){
    return matrix[centerx + x][centery + y];
}
