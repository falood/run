#include "Base.h"
#include "MaterialPool.h"
#include "MapCtrl.h"
#include <time.h>
#define random(x) (rand()%x)

MapCtrl::MapCtrl(Ogre::SceneManager *mSceneMgr, Character *mHero){
    srand((int)time(0));
    addmapn = 0;
    zoom = 1;
    side = 60;
    step = zoom * side;
    this->mSceneMgr = mSceneMgr;
    this->mHero = mHero;
    moveLeftState = moveRightState = false;
    // 加载资源到资源池列表
    // MaterialPool(mSceneMgr, name, mesh, type, direction)
    // mats["turnl1"] = new MaterialPool(mSceneMgr, "config/turnl1.cfg")
    // mats["straight1"] = new MaterialPool(mSceneMgr, "media/config/straight1.cfg");
    // mats["turnr1"] = new MaterialPool(mSceneMgr, "media/config/turnr1.cfg");
    // mats["turnl1"] = new MaterialPool(mSceneMgr, "media/config/turnl1.cfg");
    printf("START LOAD MATERIALS\n");
    mats["dir1"] = new MaterialPool(mSceneMgr, "media/config/dir1.cfg");
    printf("FINISHED LOAD MATERIALS\n");
    h = t = tt = NULL;
    add("dir1");
    add("dir1");
    add("dir1");
    add("dir1");
}
MapCtrl::~MapCtrl(){
    // 释放链表
    node *tmpNode;
    while (NULL != t){
        tmpNode = t->next;
        t->next = tmpNode->next;
        delete tmpNode;
        tmpNode = NULL;
    }
    h = t = tt = NULL;
}

void MapCtrl::add(std::string s){
    node *tmpNode = new node;
    tmpNode->mat = mats[s];
    tmpNode->next = NULL;
    tmpNode->posx = tmpNode->posz = tmpNode->direction = 0;
    tmpNode->turnFlag = false;
    tmpNode->mapNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
    tmpNode->mapNode->attachObject(tmpNode->mat->get()); // 从资源池取一个资源绑定到结点
    tmpNode->mapNode->setScale(zoom, zoom, zoom);
    if (NULL == h){
        h = t = tt = tmpNode;
        tmpNode->mapNode->setPosition(tmpNode->posx, 0, tmpNode->posz);
    }else{
        // 位置计算
        tmpNode->posx = h->posx;
        tmpNode->posz = h->posz;
        switch (h->direction){
        case 0: tmpNode->posz -= step; break;
        case 1: tmpNode->posx += step; break;
        case 2: tmpNode->posz += step; break;
        case 3: tmpNode->posx -= step; break;
        default: break;
        }
        // 结点方向设置
        tmpNode->direction = (h->direction + 4 + tmpNode->mat->getDire()) & 3;
        switch (tmpNode->direction){
        case 0: tmpNode->mapNode->setDirection(0, 0, 1); break;
        case 1: tmpNode->mapNode->setDirection(1, 0, 0); break;
        case 2: tmpNode->mapNode->setDirection(0, 0, -1); break;
        case 3: tmpNode->mapNode->setDirection(-1, 0, 0); break;
        }
        // 放置结点
        tmpNode->mapNode->setPosition(tmpNode->posx, 0, tmpNode->posz);
        // 添加到链表
        h = h->next = tmpNode;
    }
    // 释放指针
    tmpNode = NULL;
    delete tmpNode;
}

void MapCtrl::rel(){
    t = t->next;
    if (t == tt->next->next->next){
        node *tmpNode = tt;
        tt = tt->next;
        tmpNode->mapNode->detachObject(tmpNode->mat->rel()); // 释放资源
        mSceneMgr->getRootSceneNode()->removeChild(tmpNode->mapNode); // 释放结点
        // 释放指针
        tmpNode = NULL;
        delete tmpNode;
    }
}

int MapCtrl::checkState(){
    int tmpx, tmpz, state;
    Vector3 pos = mHero->getPosition();
    tmpx = (int)(pos.x - t->posx);
    tmpz = (int)(pos.z - t->posz);
    switch(t->direction){
    case 0:
        state = t->mat->checkState(tmpz, tmpx); break;
    case 1:
        state = t->mat->checkState(-1 * tmpx, tmpz); break;
    case 2:
        state = t->mat->checkState(-1 * tmpz, -1 * tmpx); break;
    case 3:
        state = t->mat->checkState(tmpx, -1 * tmpz); break;
    default:
        break;
    }
    return state;
}

void MapCtrl::mHeroMove(int state){
    switch(state){
    case 5:                     // 左缓冲区
        if(moveRightState){
            mHero->moveRight();
        }
        break;
    case 9:                     // 右缓冲区
        if(moveLeftState){
            mHero->moveLeft();
        }
        break;
    case 7:                     // 左边界出界右移
        mHero->moveRight();
        break;
    case 8:                     // 右边界出界左移
        mHero->moveLeft();
        break;
    case 6:
        break;
    default:
        if(moveLeftState){
            mHero->moveLeft();
        }
        if(moveRightState){
            mHero->moveRight();
        }
        break;
    }
}

bool MapCtrl::refresh(){
    int state = checkState();
    mHeroMove(state);
    switch(state){
    case 0:
        break;
    case 1:
        if (t->turnFlag){
            mHero->turnRight();
            t->turnFlag = false;
        }
        break;
    case 3:
        if (t->turnFlag){
            mHero->turnLeft();
            t->turnFlag = false;
        }
        break;
    case 4:
        return gameOver(4);
        break;
    case 6:
        rel();
        addmap();
        break;
    default:
        break;
    }
    return true;
}

bool MapCtrl::leftTurnEvent(){
    if (3 == t->mat->getType()){
        mHero->turnLeft();
        return true;
    }
    node *tmpNode = t;
    for (int i=0; i<2; i++){
        if (3 == tmpNode->mat->getType()){
            tmpNode->turnFlag = true;
            tmpNode = NULL;
            delete tmpNode;
            return true;
        }
        tmpNode = tmpNode->next;
    }
    tmpNode = NULL;
    delete tmpNode;
    return gameOver(1);
}

bool MapCtrl::rightTurnEvent(){
    if (1 == t->mat->getType()){
        mHero->turnRight();
        return true;
    }
    node *tmpNode = t->next;
    for (int i=0; i<2; i++){
        if (1 == tmpNode->mat->getType()){
            tmpNode->turnFlag = true;
            tmpNode = NULL;
            delete tmpNode;
            return true;
        }
        tmpNode = tmpNode->next;
    }
    tmpNode = NULL;
    delete tmpNode;
    return gameOver(2);
}

void MapCtrl::startLeftMove(){
    stopRightMove();
    moveLeftState = true;
}

void MapCtrl::startRightMove(){
    stopLeftMove();
    moveRightState = true;
}

void MapCtrl::stopLeftMove(){
    moveLeftState = false;
}

void MapCtrl::stopRightMove(){
    moveRightState = false;
}

bool MapCtrl::gameOver(int t){
    // 1左转失败 2右转失败
    printf("AAA-Game Over-%d\n", t);
    return false;
}

void MapCtrl::addmap(){
    add("dir1");
    return;
    if (addmapn > 0){
        add("straight1");
        addmapn --;
    }else{
        int r = rand() % 100;
        if (r<40){
            add("turnl1");
            addmapn = 4;
        }else if(r<80){
            add("turnr1");
            addmapn = 4;
        }else{
            add("straight1");
        }
    }
}
